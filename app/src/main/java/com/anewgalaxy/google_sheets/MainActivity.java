package com.anewgalaxy.google_sheets;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    public static final String SCRIPT_URL = "https://script.google.com/macros/s/AKfycbydIiWLZbV1Jkh4GFOfAxHUC_QMx4HVoV72C7-hrT-uTNRfRPUtER9T/exec";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set this activity's content view
        setContentView(R.layout.activity_main);

        // Set the support action bar to the respective toolbar from the layout file
        setSupportActionBar((Toolbar) findViewById(R.id.main_action_bar));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the toolbar with the menu menu_main
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Check which menu item was clicked
        //if (item.getItemId() == R.id.menu_item) {}

        return super.onOptionsItemSelected(item);

    }

}