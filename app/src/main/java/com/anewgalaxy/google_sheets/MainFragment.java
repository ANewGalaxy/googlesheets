package com.anewgalaxy.google_sheets;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class MainFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_main, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.nav_btn_add_guest).setOnClickListener(v -> {

            Intent intent = new Intent(requireActivity().getApplicationContext(), AddGuest.class);
            startActivity(intent);

        });

        view.findViewById(R.id.nav_btn_get_guests).setOnClickListener(v -> {

            Intent intent = new Intent(requireActivity().getApplicationContext(), GuestList.class);
            startActivity(intent);

        });

    }

}
