package com.anewgalaxy.google_sheets;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class GuestList extends AppCompatActivity {

    //private ProgressDialog loadingDialog;
    private ListAdapter guestListAdapter;
    private ListView guestListView;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.guest_list);

        setSupportActionBar((Toolbar) findViewById(R.id.main_action_bar));

        guestListView = findViewById(R.id.lv_guest_list);

        progressBar = findViewById(R.id.progress_bar);

        getItems();

    }

    private synchronized void getItems() {

        //loadingDialog = ProgressDialog.show(this, "Loading guest list", "Please wait");

        progressBar.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, MainActivity.SCRIPT_URL + "?action=getGuests",
                this::onResponse, this::onError);

        int socketTimeout = 50000; // 50 Seconds

        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);

        RequestQueue queue = Volley.newRequestQueue(this);

        queue.add(stringRequest);

    }

    private void onResponse(String response) {

        ArrayList<HashMap<String, String>> guestList = new ArrayList<>();

        try {

            JSONObject responseObject = new JSONObject(response);
            JSONArray jsonArray = responseObject.getJSONArray("guests");

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject o = jsonArray.getJSONObject(i);

                HashMap<String, String> guest = new HashMap<>();

                guest.put("dateAdded", o.getString("dateAdded"));
                guest.put("guestName", o.getString("guestName"));
                guest.put("emailAddress", o.getString("emailAddress"));
                guest.put("phoneNumber", o.getString("phoneNumber"));
                guest.put("birthDate", o.getString("birthDate"));
                guest.put("address", o.getString("address"));
                guest.put("city", o.getString("city"));
                guest.put("zipCode", o.getString("zipCode"));
                guest.put("nccID", o.getString("nccID"));
                guest.put("barcode", o.getString("barcode"));

                guestList.add(guest);

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

        guestListAdapter = new SimpleAdapter(this, guestList, R.layout.guest_list_row,
                new String[] {"dateAdded", "guestName", "barcode"},
                new int[] {R.id.row_tv_date_added, R.id.row_tv_guest_name, R.id.row_tv_barcode});

        guestListView.setAdapter(guestListAdapter);

        //loadingDialog.dismiss();

        progressBar.setVisibility(View.GONE);

        //Toast.makeText(GuestList.this, response, Toast.LENGTH_LONG).show();

    }

    private void onError(VolleyError error) {


    }

}
