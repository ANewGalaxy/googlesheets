package com.anewgalaxy.google_sheets;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AddGuest extends AppCompatActivity implements View.OnClickListener {

    private EditText guestName, emailAddress, phoneNumber, birthDate;
    private EditText address, city, zipCode, nccID;
    private Button buttonAddGuest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.add_guest);

        setSupportActionBar((Toolbar) findViewById(R.id.main_action_bar));

        guestName = findViewById(R.id.et_guest_name);
        emailAddress = findViewById(R.id.et_email_address);
        phoneNumber = findViewById(R.id.et_phone_number);
        birthDate = findViewById(R.id.et_birth_date);
        address = findViewById(R.id.et_address);
        city = findViewById(R.id.et_city);
        zipCode = findViewById(R.id.et_zip_code);
        nccID = findViewById(R.id.et_ncc_id);

        buttonAddGuest = findViewById(R.id.btn_add_guest);
        buttonAddGuest.setOnClickListener(this);

    }

    private synchronized void addItemToSheet() {

        final ProgressDialog LOADING = ProgressDialog.show(this, "Adding Item", "Please wait");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, MainActivity.SCRIPT_URL,
                response -> {

                    LOADING.dismiss();

                    Toast.makeText(AddGuest.this, response, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                    startActivity(intent);

                }, this::onError) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();

                params.put("sheetId", "1o3ODITw9SANtdeNtuh0LMaNxZTyzrkM3hWFT19n9tM0");
                params.put("sheetName", "Guests");

                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());

                params.put("action", "addGuest");
                params.put("dateAdded", sdf.format(new Date()).trim());
                params.put("guestName", getText(guestName));
                params.put("emailAddress", getText(emailAddress));
                params.put("phoneNumber", getText(phoneNumber));
                params.put("birthDate", getText(birthDate));
                params.put("address", getText(address));
                params.put("city", getText(city));
                params.put("zipCode", getText(zipCode));
                params.put("nccID", getText(nccID));
                params.put("barcode", "TODO-1234");

                return params;

            }


        };

        int socketTimeout = 50000; // 50 Seconds

        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);

        RequestQueue queue = Volley.newRequestQueue(this);

        queue.add(stringRequest);


    }

    private void onError(VolleyError error) {


    }

    private String getText(EditText editText) {

        return editText.getText().toString().trim();

    }

    @Override
    public void onClick(View v) {

        if (v.equals(buttonAddGuest)) {

            addItemToSheet();

            /*if (guestName.getText().length() == 0 ||
                emailAddress.getText().length() == 0 ||
                phoneNumber.getText().length() == 0 ||
                birthDate.getText().length() == 0 ||
                address.getText().length() == 0 ||
                city.getText().length() == 0 ||
                zipCode.getText().length() == 0 ||
                nccID.getText().length() == 0) {

                Toast.makeText(AddItem.this, "Please fill in all fields!", Toast.LENGTH_SHORT).show();

            } else

                addItemToSheet();*/

        }

    }
}
